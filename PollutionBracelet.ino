 
#include <Adafruit_NeoPixel.h>

//#define DEBUG

#define LED_PIN 6

#define LEDS 16
#define CLIP_FIRST_LEDS 1 // Prevent the first N LEDs from being basically always on because you always have a few particles in the air
#define LED_ROTATION 5
#define LEN 9
#define windowSize 10
 
Adafruit_NeoPixel leds = Adafruit_NeoPixel(LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);

float scalePower = .5;

float aveArr[windowSize];
int loopcnt = 0;
unsigned char incomingByte = 0; // for incoming serial data
unsigned char buf[LEN];
int PM2_5Val = 0;
int PM10Val = 0;
int PM2_5ave = 0;

void setup() {
  Serial.begin(9600);
  leds.begin();
  
  showMonoResult(16);
  leds.show();
  
  delay(1000);
  
  clearLeds();
  leds.show();
}
 
void loop() {
  int i;
  unsigned char checksum;

  // send data only when you receive data:
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();
    if (incomingByte == 0xAA) {
      Serial.readBytes(buf, LEN);
      if ((buf[0] == 0xC0) && (buf[8] == 0xAB)) {
        PM2_5Val = ((buf[2] << 8) + buf[1]) / 10;
        PM10Val  = ((buf[4] << 8) + buf[3]) / 10;

        #ifdef DEBUG
          // Simulate the whole pollution range
          PM2_5Val = (loopcnt % 20) * 12;
          PM10Val  = (loopcnt % 20) * 12;
        #endif

        processNewData();
        
        loopcnt++;
      }
    }
  }
}

static void processNewData() {
  int displayValue = max(0, pow((PM2_5Val + PM10Val) / 2, scalePower) - CLIP_FIRST_LEDS); // Subtraction to avoid using the first LED(s) basically all the time
  //int displayValue = (PM2_5Val + PM10Val) / 24;

  #ifdef DEBUG
    Serial.print("PM2: ");
    Serial.print(PM2_5Val);
    Serial.print(" \tPM10: ");
    Serial.print(PM10Val);
    Serial.print(" \tDisplay: ");
    Serial.println(displayValue);
  #endif
  
  clearLeds();
  showMonoResult(displayValue);
  leds.show();
}

static void showMonoResult(int value) {
  uint32_t color;
  value = min(LEDS - 1, value);

  for(uint16_t i = 0; i <= value; i++) {
    if(i < 3) {
      color = leds.Color(0, 8, 0);
    } else if(i < 6) {
      color = leds.Color(6, 8, 0);
    } else {
      color = leds.Color(8, 0, 0);
    }
    
    leds.setPixelColor((LEDS - i + LED_ROTATION) % LEDS, color);
  }
}

static void clearLeds() {
  for(uint16_t i = 0; i < leds.numPixels() + 1; i++) {
      leds.setPixelColor(i, 0);
  }
}

